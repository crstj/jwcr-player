--Quick drop calls

drop table JWCR_ArtistLookup;
drop table JWCR_Albums;
drop table JWCR_SongLookUp;
drop table JWCR_Songs;
drop table JWCR_Played;
drop table JWCR_Artists;
drop table JWCR_SuggestionFeed;


CREATE TABLE JWCR_Artists (
  ArtistID Number(10),
  ArtistName varchar2(50),
  ArtistPic varchar2(50),
  CONSTRAINT artist_id_pk PRIMARY KEY(ArtistID)

);

CREATE TABLE JWCR_Played (
  PlayID Number(10),
  ArtistID  Number(10),
  Time_Played varchar2(10),
  SongID Number(10),
    CONSTRAINT played_id_pk PRIMARY KEY(PlayID),
    CONSTRAINT played_artist_id_fk FOREIGN KEY (ArtistID)
        REFERENCES JWCR_Artists (ArtistID)

);

CREATE TABLE JWCR_Songs (
  SongID Number(10),
  SongName varchar2(100),
  SongPic varchar2(50),
  Genre varchar2(50),
  PlayID Number(10),
    CONSTRAINT songs_id_pk PRIMARY KEY(SongID),
    CONSTRAINT songs_play_id_fk FOREIGN KEY(PlayID)
        REFERENCES JWCR_Played (PlayID)

);

CREATE TABLE JWCR_SongLookUp (
  AlbumID Number(10),
  SongID Number(10),
    CONSTRAINT slu_album_id_pk PRIMARY KEY(AlbumID),
    CONSTRAINT slu_song_id_fk FOREIGN KEY (SongID)
        REFERENCES JWCR_Songs (SongID)

);

CREATE TABLE JWCR_Albums (
  AlbumID Number(10),
  AlbumName varchar2(100),
  AlbumPic varchar2(50),
    CONSTRAINT albums_id_pk PRIMARY KEY(AlbumID),
    CONSTRAINT albums_slu_id_fk FOREIGN KEY (AlbumID)
        REFERENCES JWCR_SongLookup (AlbumID)
  
);

CREATE TABLE JWCR_ArtistLookUp (
  ArtistID  Number(10),
  AlbumID  Number(10),
    CONSTRAINT artlu_artist_ID_pk PRIMARY KEY(ArtistID),
    CONSTRAINT alrtlu_id_fk FOREIGN KEY (AlbumID)
        REFERENCES JWCR_Albums (AlbumID)

);

CREATE TABLE JWCR_SuggestionFeed (

ArtistID  Number(10),
SongID  Number(10),
Genre varchar2(50),
Played_Weight Number(10)

);

