Insert into JWCR_Artists Values(01,'Led Zepplin','Led-Zeppelin.jpg');
Insert into JWCR_Artists Values(02,'Van Halen','Van-Halen.jpg');
Insert into JWCR_Artists Values(03,'Aerosmith','Aerosmith.jpg');
Insert into JWCR_Artists Values(04,'Rush','Rush.jpg');
Insert into JWCR_Artists Values(05,'Creedence Clearwater Revival','CCR.jpg');

Insert into JWCR_Albums Values(01,'Physical Graffiti','Led_Zeppelin_-_Physical_Graffiti.jpg');
Insert into JWCR_Albums Values(02,'Van Halen','Van-Halen-Runnin-With-The-Devil.jpg');
Insert into JWCR_Albums Values(03,'Toys in the Attic','Aerosmith-Toys-in-the-Attic.jpg');
Insert into JWCR_Albums Values(04,'Moving Pictures','Rush-Moving-Pictures.jpg');
Insert into JWCR_Albums Values(05,'Willy and the Poor Boys','CCR-Willy-and-the-Poor-Boys.jpg');

Insert into JWCR_ArtistLookUp Values(01,01);
Insert into JWCR_ArtistLookUp Values(02,02);
Insert into JWCR_ArtistLookUp Values(03,03);
Insert into JWCR_ArtistLookUp Values(04,04);
Insert into JWCR_ArtistLookUp Values(05,05);

Insert into JWCR_Genre Values(01,'Classic Rock');
Insert into JWCR_Genre Values(02,'Hard Rock');
Insert into JWCR_Genre Values(03,'Rock');
Insert into JWCR_Genre Values(04,'Progressive Rock');
Insert into JWCR_Genre Values(05,'Rock and Roll');

Insert into JWCR_GenreLookUp Values(01,01);
Insert into JWCR_GenreLookUp Values(02,02);
Insert into JWCR_GenreLookUp Values(03,03);
Insert into JWCR_GenreLookUp Values(04,04);
Insert into JWCR_GenreLookUp Values(05,05);

Insert into JWCR_Songs Values(01,'Kashmir','Kashmir.jpg',01);
Insert into JWCR_Songs Values(02,'Runnin with the devil','RunninWithTheDevil.jpg',02);
Insert into JWCR_Songs Values(03,'Sweet Emotion','SweetEmotion.jpg',03);
Insert into JWCR_Songs Values(04,'Tom Sawyer','TomSawyer.jpg',04);
Insert into JWCR_Songs Values(05,'Fortunate Son','FortunateSon.jpg',05);

Insert into JWCR_Played Values(01,01,'8:37',01);
Insert into JWCR_Played Values(02,02,'3:35',01);
Insert into JWCR_Played Values(03,03,'3:09',03);
Insert into JWCR_Played Values(04,04,'4:30',04);
Insert into JWCR_Played Values(05,05,'2:21',05);

Insert into JWCR_SuggestionFeed Values(01,01,'Classic Rock',10);



--JWCR_Artists; JWCR_Albums; JWCR_ArtistLookup; 
--JWCR_Genre; JWCR_GenreLookUp;
--JWCR_Played; JWCR_SuggestionFeed;

--Insert 1
INSERT INTO JWCR_ARTISTS
    VALUES(0000001001, 'Powerwolf', null);
INSERT INTO JWCR_ALBUMS
    VALUES(1000000000, 'The Sacrament of Sin', null);
INSERT INTO JWCR_ARTISTLOOKUP
    VALUES(0000001001, 1000000000);
INSERT INTO JWCR_GENRE
    VALUES(0000000011, 'Power Metal');
INSERT INTO JWCR_GENRELOOKUP
    VALUES(1000000000, 0000000011);
INSERT INTO JWCR_SONGS
    VALUES(0000100000, 'Incense and Iron', null, 0000000011);
INSERT INTO JWCR_PLAYED
    VALUES(0000000111, 0000000001, null, 0000100000);
--Will later figure out suggestion feed and how to fill in

--Insert 2
INSERT INTO JWCR_ARTISTS
    VALUES(0000001002, 'Sabaton', null);
INSERT INTO JWCR_ALBUMS
    VALUES(2000000000, 'Heros', null);
INSERT INTO JWCR_ARTISTLOOKUP
    VALUES(0000001002, 2000000000);
--INSERT INTO JWCR_GENRE
    --VALUES(0000000011, 'Power Metal');
--Do not need this insert again because already in table
INSERT INTO JWCR_GENRELOOKUP
    VALUES(2000000000, 0000000011);
INSERT INTO JWCR_SONGS
    VALUES(0000200000, 'Resist and Bite', null, 0000000011);
INSERT INTO JWCR_PLAYED
    VALUES(0000000222, 0000000002, null, 0000200000);
    
--Insert 3
INSERT INTO JWCR_ARTISTS
    VALUES(0000001003, 'Metallica', null);
INSERT INTO JWCR_ALBUMS
    VALUES(3000000000, '...And Justice for All', null);
INSERT INTO JWCR_ARTISTLOOKUP
    VALUES(0000001003, 3000000000);
INSERT INTO JWCR_GENRE
    VALUES(0000000022, 'Heavy Metal');
INSERT INTO JWCR_GENRELOOKUP
    VALUES(3000000000, 0000000022);
INSERT INTO JWCR_SONGS
    VALUES(0000300000, 'Eye of the Beholder', null, 0000000022);
INSERT INTO JWCR_PLAYED
    VALUES(0000000333, 0000000003, null, 0000300000);
    
--Insert 4
INSERT INTO JWCR_ARTISTS
    VALUES(0000001004, 'Disturbed', null);
INSERT INTO JWCR_ALBUMS
    VALUES(4000000000, 'Indestructible', null);
INSERT INTO JWCR_ARTISTLOOKUP
    VALUES(0000001004, 4000000000);
INSERT INTO JWCR_GENRE
    VALUES(0000000033, 'Alternative Rock');
INSERT INTO JWCR_GENRELOOKUP
    VALUES(4000000000, 0000000033);
INSERT INTO JWCR_SONGS
    VALUES(0000400000, 'Inside the Fire', null, 0000000033);
INSERT INTO JWCR_PLAYED
    VALUES(0000000333, 0000000003, null, 0000300000);
    
--Insert 5
INSERT INTO JWCR_ARTISTS
    VALUES(0000001005, 'Yiruma', null);
INSERT INTO JWCR_ALBUMS
    VALUES(5000000000, 'Yiruma and Piano', null);
INSERT INTO JWCR_ARTISTLOOKUP
    VALUES(0000001005, 5000000000);
INSERT INTO JWCR_GENRE
    VALUES(0000000044, 'New Age');
INSERT INTO JWCR_GENRELOOKUP
    VALUES(5000000000, 0000000044);
INSERT INTO JWCR_SONGS
    VALUES(0000500000, 'River Flows in You', null, 0000000044);
INSERT INTO JWCR_PLAYED
    VALUES(0000000555, 0000000005, null, 0000500000);
    
    
select * from JWCR_ARTISTS;
select * from JWCR_ALBUMS;
select * from JWCR_ARTISTLOOKUP;
select * from JWCR_GENRE;
select * from JWCR_GENRELOOKUP;
select * from JWCR_SONGS;
select * from JWCR_Played;
select * from jwcr_suggestionfeed;
    